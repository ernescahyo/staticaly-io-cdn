# Bitbucket + Statically

Loads file from Bitbucket fully optimized and deliver fast with the right of content type.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

* Copy file URL from Bitbucket

![ImgName](https://statically.io/images/docs/bitbucket-docs-0.png)

* Visit Statically.io and paste file URL from Bitbucket into Statically form
![ImgName](https://statically.io/images/docs/bitbucket-docs-1.png)
* [Learn More Setting](https://statically.io/bitbucket/)
